<?php get_header();?>
                       
                       <?php
                        $office_custom_post = null;
        
        
                            $office_custom_post = new WP_Query(array(
                            'post_type'=>'page',
                            'posts_per_page'=>-1,

                            ));

                            if($office_custom_post->have_posts()){
                              
                                while($office_custom_post->have_posts()){
                                
                                    $office_custom_post->the_post();
                                    if(get_the_ID() == 201 ){
                                        $page_thumb = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ) , 'full' );?>  

                                <div class="row container-kamn">  
                                    <img src="<?php echo $page_thumb[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
                                </div>

                               
                                
                         <?php }
                             }
             
                            }else{
                                echo "no post";
                            }
                        
                         wp_reset_postdata();

                        ?>

       <!--End Header -->
    


    <!-- Main Container -->
    <div id="banners"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-9"> 
                   
                   
                     <?php 
                          $prf= '_office_master_';

                            if(have_posts()){
                            
                                while(have_posts()){
                                 
                                      the_post();
                                    $post_icon = get_post_meta(get_the_ID(),$prf.'post_icon',true);
                                    ?>
                                    
                                    
                                        <div class="blog-post">
                                        <h1 class="blog-title">
                                            <a href="<?php the_permalink(); ?>"><i class="fa  <?php echo $post_icon; ?>"></i></a>
                                            <a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>
                                        </h1>
                                        <br>
                                         <a href="<?php the_permalink(); ?>"><?php echo the_post_thumbnail('post-th'); ?></a>
                                        <br>
                                        <?php echo cExcerpt(60,"Read Here");
                                            
                                            
                                            ?>
                                            
                                        
                                        <div>
                                            <span class="badge">Posted <?php echo get_the_date('Y-m-d H:i:s'); ?></span>
                                            <div class="pull-right">
                                               <?php the_tags('<span class="label label-default">','</span><span class="label label-default">','</span>' );?>

                                            </div>         
                                        </div>
                                    </div>
                                    <hr>
                                   
                                    
                                   
                                    
                              <?php  }
                                
                                the_posts_pagination(array(
                                
                                'prev_text' => '«',
                                'next_text' => '»',
                                    'mid_size' => 3
                                   
                                
                                ));
                                
                            }else{
                                echo "no post";
                            }
                        
                         wp_reset_postdata();

                        ?>

                    
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Sign in </strong></h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="<?php echo site_url(); ?>/wp-login.php" method="post" >
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username or Email</label>
                                    <input type="text" class="form-control" style="border-radius:0px" id="exampleInputEmail1" placeholder="Enter username" name="log">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password <a href="<?php echo site_url(); ?>/wp-login.php?action=lostpassword">(forgot password)</a></label>
                                    <input type="password" class="form-control" style="border-radius:0px" id="exampleInputPassword1" placeholder="Password" name="pwd">
                                </div>
                                <input type="hidden" name="redirect_to" value="<?php echo site_url(); ?>" >
                                <button type="submit" class="btn btn-sm btn-default" name="wp-submit">Sign in</button>
                            </form>
                        </div>
                    </div>

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                       
                       
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                           
                        <?php 
                         $office_custom_post = null;
                          
        
                            $office_custom_post = new WP_Query(array(
                            'post_type'=>'slider',
                            'posts_per_page'=>-1,
                                'order' => 'ASC'

                            ));

                            if($office_custom_post->have_posts()){
                                $x=0;
                                while($office_custom_post->have_posts()){
                                    $x++;
                                    $office_custom_post->the_post();
                                   
                                    ?>
                                    
                                     <!-- Begin single Slide item-->
                                    <div class="item <?php if($x==1){ echo 'active';} ?>">
                                        <?php the_post_thumbnail('side-bar-slide'); ?>
                                        
                                    </div>
                                    <!-- End single Slide item -->
                                   
                                    
                              <?php  }
                                
                            }else{
                                echo "no post";
                            }
                        
                         wp_reset_postdata();

                        ?>
                           
                           
                           
                            
                        </div>
                        <!-- Controls -->
                        
                         <!-- Indicators -->
                        <ol class="carousel-indicators">
                           
                           
                           <?php
                        for($i=0;$i<$x;$i++){ ?>
                           
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0){ echo 'active';} ?>"></li>
                           
                           <?php  }
 
                        ?> 
                           
                           
                        </ol>
                        
                    </div>
                </div>
                
                
                <div class="test_widgets" style="float:right">
                    <?php 
                    if(!dynamic_sidebar('office_right_sidebar')){
                        echo "No POST";
                    }
                    
                    
                    ?>
                     
                </div>
                
                
                
            </div>
        </div>
       
        <!--End Main Container -->

<?php get_footer();?>
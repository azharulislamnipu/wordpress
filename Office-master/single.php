<?php get_header();?>
 
       <?php
                        $office_custom_post = null;
        
        
                            $office_custom_post = new WP_Query(array(
                            'post_type'=>'page',
                            'posts_per_page'=>-1,

                            ));

                            if($office_custom_post->have_posts()){
                              
                                while($office_custom_post->have_posts()){
                                
                                    $office_custom_post->the_post();
                                    if(get_the_ID() == 201 ){
                                        $page_thumb = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ) , 1500,300 );?>  

                                <div class="row container-kamn">  
                                    <img src="<?php echo $page_thumb[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" height="50%"> 
                                </div>

                               
                                
                         <?php }
                             }
             
                            }else{
                                echo "no post";
                            }
                        
                         wp_reset_postdata();

                        ?>

       <!--End Header -->



    <div id="banners"></div>
    <div class="container">   
        <div class="row">
        
            <div class="col-sm-12 col-md-12">
             
                <?php 
                      $prf= '_office_master_';

                            if(have_posts()){
                            
                                while(have_posts()){
                                 
                                      the_post();
                                    $post_icon = get_post_meta(get_the_ID(),$prf.'post_icon',true);
                                    ?>
                                    
                                    
                                        <div class="blog-post">
                                        <h1 class="blog-title">
                                            <i class="fa <?php echo $post_icon; ?>"></i>
                                          <?php echo the_title(); ?>
                                        </h1>
                                        <br>
                                        <?php echo the_post_thumbnail('post-th'); ?>
                                        <br>
                                        <?php echo the_content(); ?>
                                        
                                        <div>
                                            <span class="badge">Posted <?php echo get_the_date('Y-m-d H:i:s'); ?></span>
                                            <div class="pull-right">
                                               <?php the_tags('<span class="label label-default">','</span><span class="label label-default">','</span>' );?>

                                            </div>         
                                        </div>
                                    </div>
                                    <hr>
                                   
                                    
                                   
                                    
                              <?php  }
                                
                               
                                
                            }else{
                                echo "no post";
                            }
                        
                         wp_reset_postdata();
     
                ?>
               
                 
         
              
            </div>  
        </div>    
    </div>  

    <!--End Main Container -->

  <?php get_footer();?>
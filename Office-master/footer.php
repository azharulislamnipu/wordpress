<?php global $office_master;?>
  
   
   
    <!-- Footer -->
    <footer style="background:<?php echo $office_master['footer_top_bg'];?>"> 
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3><i class="fa <?php  echo $office_master['contact_icon']; ?>"></i><?php echo " ".$office_master['contact_title']; ?></h3>
                    <p class="footer-contact">
                        <?php  echo $office_master['contact_desc']; ?>
                    </p>
                </div>
                <div class="col-md-4">
                    <h3><i class="fa <?php  echo $office_master['links_icon']; ?>"></i> <?php  echo " ".$office_master['links_title']; ?></h3>
                    
                    <?php 
                        
                        if(is_array($office_master['links_url'])){
                          foreach($office_master['links_url'] as $linksValue){ ?>

                             <p> <a href="<?php echo $linksValue['url'];   ?>"> <?php echo $linksValue['title']; ?></a></p>
                                
                                 
                        <?php } }
    
                    ?>
            
                </div>
              <div class="col-md-4">
                <h3><i class="fa <?php  echo $office_master['social_icon'] ?>"></i> <?php  echo " ".$office_master['social_title'] ?></h3>
                <div id="social-icons">
                   
                    <?php 
                        
                        if(is_array($office_master['social_link'])){
                          foreach($office_master['social_link'] as $linksValue){ ?>

                           
                             <a href="<?php echo $linksValue['url']; ?>" class="btn-group google-plus">
                                <i class="fa <?php echo $linksValue['title']; ?>"></i>
                             </a>
                                 
                        <?php } }
    
                    ?>
                   
                </div>
              </div>    
        </div>
      </div>
    </footer>

    
    <div class="copyright text center" style="background:<?php echo $office_master['footer_copy_writ_bg'];?>">
       
       <?php  global $office_master; echo $office_master['copywrite_txt']; ?>
        
    </div>


    <?php wp_footer(); ?>
  </body>
</html>

<?php

function office_theme_init(){
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
    add_image_size('slide-img',1500,500,true);
    add_image_size('side-bar-slide',292,292,true);
    add_image_size('team-member',100,80,true);
    add_image_size('post-th',848,497,true);
    

	register_nav_menus(array(

	// 	'main_menu' => 'Main Menu',
	 	'primary_menu' => 'Primary Menu',
	 	'secondary_menu' => 'Secondary Menu'


		));


}

add_action('after_setup_theme','office_theme_init');

  



function office_master_css_js(){
	wp_enqueue_style('google-font-1','//fonts.googleapis.com/css?family=Open+Sans:400,300',null,'v1.0','all');
	wp_enqueue_style('google-font-2','//fonts.googleapis.com/css?family=PT+Sans',null,'v1.1','all');
	wp_enqueue_style('google-font-3','//fonts.googleapis.com/css?family=Raleway',null,'v1.2','all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/bootstrap/css/bootstrap.css',null,'v3.1.1','all');
	wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css',null,'4.1.1','all');
	wp_enqueue_style('style',get_template_directory_uri().'/assets/css/style.css',null,'1.0.0','all');
	wp_enqueue_style('animate',get_template_directory_uri().'/assets/css/animate.min.css',null,'4.1.1','all');

	wp_enqueue_style('main-style',get_stylesheet_uri(),null,'v.1.1','all');

	
    
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap',get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js','jquery',null,true);
    wp_enqueue_script('wow-js',get_template_directory_uri().'/js/wow.min.js','jquery',null,true);

}
add_action('wp_enqueue_scripts','office_master_css_js');

function footer_extra_script(){
    ?>
    
    
    <script>
      new WOW().init();
    </script>

<?php
}

add_action('wp_footer','footer_extra_script', 30);

function office_master_fallback_menu(){ ?>
    
    <ul class="nav navbar-nav pull-right">
                    <li class="active">
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Blog</a>
                    </li>
                    <li>
                        <a href="#">Team</a>
                    </li>
                    <li>
                        <a href="#"><span>Contact</span></a>
                    </li>
                </ul>    
    
<?php }





include_once('inc/custom_post.php');
include_once('inc/custom_sortcode.php');
include_once('inc/cmb2_custom_field.php');
require_once('inc/redux-framework/redux-framework.php');
require_once('inc/office_master_theme_option.php');
include_once('inc/office_master_plugin_activation.php');
include_once('inc/custom_widgets.php');
include_once('inc/custom_register_field.php');





function my_add_mce_button() {
	// check user permissions
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'office_master_tinymce_plugin' );
		add_filter( 'mce_buttons', 'office_master_register_mce_button' );
	}
}
add_action('admin_head', 'my_add_mce_button');

// Declare script for new button
function office_master_tinymce_plugin( $plugin_array ) {
	$plugin_array['office_first_button'] = $plugin_array['office_second_button']  = get_template_directory_uri().'/js/office-master-mce-button.js';
	
	return $plugin_array;
}

// Register new button in the editor
function office_master_register_mce_button( $buttons ) {
	array_push( $buttons, 'office_first_button' );
	array_push( $buttons, 'office_second_button' );
	return $buttons;
}




function cExcerpt($limit, $readmore = 'Read More'){
    $limit = $limit+1;
   // $content = 
    $var = explode(' ' ,strip_tags(get_the_content()), $limit );
    
    if(count($var) >= $limit){
        array_pop($var);
    }
    
    array_push($var,'<a href="'. get_the_permalink().'">'.$readmore.'</a>');
    $var = implode(' ',$var);
    return $var;
}








?>
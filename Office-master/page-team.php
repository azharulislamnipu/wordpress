<?php get_header();

if(have_posts()){
    the_post();
  $page_thumb = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ) , 'full' );?>
     
      <div class="row container-kamn">  
        <img src="<?php echo $page_thumb[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
    </div>
 
    <!--End Header -->
    
<?php }

?>


    <!-- Main Container -->

    <div id="banners"></div>
    <div class="container">
        <div class="row">
           
            <?php 
                         $office_custom_post = null;
                          $prf= '_office_master_';
        
                            $office_custom_post = new WP_Query(array(
                            'post_type'=>'team',
                            'team_category'=> 'Web developer,Designer',
                            'team_tag' =>'first,second',
                            'posts_per_page'=>-1,
                            'order' => 'ASC'

                            ));

                            if($office_custom_post->have_posts()){
                                while($office_custom_post->have_posts()){
                                    $office_custom_post->the_post();
                                    $team_designation = get_post_meta(get_the_ID(),$prf.'team_designation',true);
                                    $team_block_color = get_post_meta(get_the_ID(),$prf.'blockqoute_color',true);
                                $animation_type = get_post_meta(get_the_ID(),$prf.'anymation_type_class',true);
                                    ?>
                                    
                              
                                        <div class="col-md-6">
                                            <div class="blockquote-box <?php echo $team_block_color; ?> animated wow <?php echo $animation_type; ?> clearfix">
                                                <div class="square pull-left">
                                                   <?php echo the_post_thumbnail('team-member'); ?>
                                                  
                                                </div>
                                                <h4>
                                                    <?php echo the_title();?>
                                                </h4>
                                                <p>
                                                    <?php echo $team_designation;?>
                                                </p>
                                            </div>
                                        </div>
                                  
                                   
                                    
                              <?php  }
                                
                            }else{
                                echo "no post";
                            }
                        
                    wp_reset_postdata();

                ?>
           
 
            
        </div>
    </div>
    <!--End Main Container -->








<?php get_footer();?>
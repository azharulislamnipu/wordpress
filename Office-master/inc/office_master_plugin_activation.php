<?php
//require_once('class-tgm-plugin-activation.php');


require get_template_directory() . '/inc/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'perth_recommend_plugin' );
function perth_recommend_plugin() {

    $plugins = array(
        array(
            'name'               => 'Regenerate Thumbnails',
            'slug'               => 'regenerate_thumbnails',
            'required'           => true,
            'force_activation' => true,
            'force_deactivation' => true,
            'source'             => get_stylesheet_directory() . '/plugins/regenerate-thumbnails.zip'
        ),
        
       array(
            'name'               => 'Contac Form 7',
            'slug'               => 'contact_form_7',
            'required'           => true,
             'force_activation' => true,
            'force_deactivation' => true,
             'source'             => get_stylesheet_directory() . '/plugins/contact-form-7.zip'
           
        ),
        
        
         array(
            'name'               => 'contact form 7 to database extension',
            'slug'               => 'contact_form_7_to_database_extension',
            'required'           => true,
            'force_activation' => true,
            'force_deactivation' => true,
            'source'    => get_stylesheet_directory() . '/plugins/contact-form-7-to-database-extension.zip'
        ),
       
    );
    $config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => get_template_directory_uri().'/plugins/contact-form-7.zip',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',     
        );

    tgmpa( $plugins);

}


?>
<?php

if ( file_exists( __DIR__ . '/cmb2/init.php' ) ) {
  require_once __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
  require_once __DIR__ . '/CMB2/init.php';
}



add_action('cmb2_admin_init','office_master_cmb2');
function office_master_cmb2(){
    
    $prefix = '_office_master_';
    $service_item = new_cmb2_box( array(
		'id'            => 'service_metabox',
		'title'         => __( 'Service Metabox', 'office_master' ),
		'object_types'  => array( 'services', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );
    
    
    $service_item->add_field( array(
		'name'       => __( 'Service Icon', 'office_master' ),
		'desc'       => __( 'Write here service icon\'s fontawesome name ', 'office_master' ),
		'id'         => $prefix.'service_icon',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		 'repeatable'      => true,
	) );
    
    
     $service_item->add_field( array(
		'name'       => __( 'Service Description', 'office_master' ),
		'desc'       => __( 'Write here your service description ', 'office_master' ),
		'id'         => $prefix.'service_discription',
		'type'       => 'textarea',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );
    
    
     $service_item->add_field( array(
		'name'       => __( 'Service Link Title', 'office_master' ),
		'desc'       => __( 'Write here your service link title ', 'office_master' ),
		'id'         => $prefix.'service_link_title',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );
    
    
    
     $service_item->add_field( array(
		'name'       => __( 'Service Link Url', 'office_master' ),
		'desc'       => __( 'Write here your service link url: http://www.sample.com ', 'office_master' ),
		'id'         => $prefix.'service_link_url',
		'type'       => 'text_url',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );
    
    
     $service_item->add_field( array(
		'name'       => __( 'Animation Style', 'office_master' ),
		'desc'       => __( 'Write here your service animation ', 'office_master' ),
		'id'         => $prefix.'animation_type',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );
    
    
    
    
    
    //new object start over here
    
     $slider_item = new_cmb2_box( array(
		'id'            => 'slider_metabox',
		'title'         => __( 'Slider Metabox', 'office_master' ),
		'object_types'  => array( 'slider', 'services'), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );
    
    
     $slider_item->add_field( array(
		'name'       => __( 'Slider Caption', 'office_master' ),
		'desc'       => __( 'Write here your slider caption name ', 'office_master' ),
		'id'         => $prefix.'slider_caption',
		'type'       => 'text',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );
    
     $special_item = new_cmb2_box( array(
            'id'            => 'Special_metabox',
            'title'         => __( 'Special Metabox', 'office_master' ),
            'object_types'  => array( 'page'), // Post type
            'show_on' => array('key'=>'id','value'=>'117'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ) );


         $special_item->add_field( array(
            'name'       => __( 'Special Caption', 'office_master' ),
            'desc'       => __( 'Write here your Special caption name ', 'office_master' ),
            'id'         => $prefix.'Special_caption',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        ) );

    
    
      $team_member = new_cmb2_box( array(
            'id'            => 'Team_metabox',
            'title'         => __( 'Team Metabox', 'office_master' ),
            'object_types'  => array( 'team'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ) );


         $team_member->add_field( array(
            'name'       => __( 'Team Member Designation', 'office_master' ),
            'desc'       => __( 'Write here your Team Member Designation', 'office_master' ),
            'id'         => $prefix.'team_designation',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        ) );
    
     $team_member->add_field( array(
            'name'       => __( 'BlockQoute Color', 'office_master' ),
            'desc'       => __( 'Write Here Your BlockQoute Color Name', 'office_master' ),
            'id'         => $prefix.'blockqoute_color',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', // functBlockQoute Color Nameion should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        ) );
    
    
     $team_member->add_field( array(
            'name'       => __( 'Animation Type', 'office_master' ),
            'desc'       => __( 'Write here your animation class name', 'office_master' ),
            'id'         => $prefix.'anymation_type_class',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        ) );
    
     $post_icon = new_cmb2_box( array(
            'id'            => 'Post_Icon_metabox',
            'title'         => __( 'Post Icon MetaBox', 'office_master' ),
            'object_types'  => array( 'post'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ) );


         $post_icon->add_field( array(
            'name'       => __( 'Post Icon', 'office_master' ),
            'desc'       => __( 'Write here your post icon fontawesome class name', 'office_master' ),
            'id'         => $prefix.'post_icon',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        ) );
    
    
    
    
    
     $about_page_group = new_cmb2_box( array(
            'id'            => 'group_page_metabox',
            'title'         => __( 'Group Page MetaBox', 'office_master' ),
            'object_types'  => array( 'page'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
          'show_on' => array('key'=>'id','value'=>'31'),
        ) );

      $about_group_para = $about_page_group->add_field( array(
            'name'       => __( 'Groupable Field', 'office_master' ),
            'id'         => $prefix.'about_group_meta_field',
            'type'       => 'group',
            'show_on_cb' => 'cmb2_hide_if_no_cats', 
            // 'repeatable'      => true,
        ) );
    $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'Heading', 'office_master' ),
            'id'         => $prefix.'heading',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', 
            // 'repeatable'      => true,
        ) );
    $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'About Description Field', 'office_master' ),
            'id'         => $prefix.'about_description',
            'type'       => 'textarea',
            'show_on_cb' => 'cmb2_hide_if_no_cats', 
            // 'repeatable'      => true,
        ) );
    $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'A tag hash link', 'office_master' ),
            'id'         => $prefix.'hash_link',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', 
             'repeatable'      => true,
        ) );
    
    $about_page_group->add_group_field($about_group_para, array(
            'name'       => __( 'A tag hash link title', 'office_master' ),
            'id'         => $prefix.'hash_link_title',
            'type'       => 'text',
            'show_on_cb' => 'cmb2_hide_if_no_cats', 
             'repeatable'      => true,
        ) );
    
    
    
}


?>
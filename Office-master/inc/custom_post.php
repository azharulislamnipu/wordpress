<?php
function office_custom_post(){
     register_post_type('slider',array(
    
    'labels' => array(
        'name' => 'Main Slider',
        'menu_name' => 'Slider Menu',
        'all_items' => 'All Slider',
        'add_new' => 'Add New Slide',
        'add_new_item' => 'Add New Slide item'
         ),
    'public'=>true,
    'supports' => array(
        'title','thumbnail','revisions','custom-fields','page-attributes'
     )
    ));
    
    
    register_post_type('services',array(
    
    'labels' => array(
        'name' => 'Service',
        'menu_name' => 'Service Menu',
        'all_items' => 'All Service',
        'add_new' => 'Add New Service',
        'add_new_item' => 'Add New Service item'
         ),
    'public'=>true,
    'supports' => array(
        'title','revisions','custom-fields','page-attributes'
     )
    ));
    
     register_post_type('team',array(
    
    'labels' => array(
        'name' => 'Team',
        'menu_name' => 'Team Menu',
        'all_items' => 'All Team Member',
        'add_new' => 'Add New Team Member',
        'add_new_item' => 'Add New Team Member'
         ),
    'public'=>true,
    'supports' => array(
        'title','revisions','page-attributes','thumbnail'
     )
    ));
    
    
    register_taxonomy('team_category','team',array(
            'label' => 'Team Catagory',
            'labels' => array(
                        'name' => 'Team Cataory',
                        'menu_name' => 'Team Catagory',
                        'add_new_item' => 'Add New Team Catagory'
                         ),
            'hierarchical' => true,
            'show_admin_column' => true
    
    
            ));
    
    register_taxonomy('team_tag','team',array(
            'label' => 'Team Tags',
            'labels' => array(
                        'name' => 'Team Tags',
                        'menu_name' => 'Team Tags',
                        'add_new_item' => 'Add New Team\'s Tags'
                         ),
            'show_admin_column' => true
    
    
            ));
    
}
add_action('init','office_custom_post');
?>
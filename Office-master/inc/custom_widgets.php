<?php

class OfficeSlider extends WP_Widget{
    
   public function  __construct(){
        parent::__construct(
            'office_slider',
            'Office Slider',
            array(
                'description' => 'This is widgets for slider'
            
            )
        
        );
    }
    
        public function form($instance){?>
          
          <label for="<?php echo $this->get_field_id('slider'); ?>">ID</label>
          <input type="text" name="<?php echo $this->get_field_name('slider_id'); ?>" id="<?php echo $this->get_field_id('slider'); ?>" value="<?php echo $instance['slider_id'] ?>" class="widefat">
           
           
          <?php   }
    
            public function update($new,$old){
                
                $old['slider_id'] = $new['slider_id'];
                return $old;

            }        
    
    
    
            public function widget( $args, $instance ) {
                       echo do_shortcode('[myslider id="'.$instance['slider_id'].'"]');
            }
    
            

}


class OfficePost extends WP_Widget{
    
   public function  __construct(){
        parent::__construct(
            'office_post',
            'Office Post',
            array(
                'description' => 'This is widgets for post'
            
            )
        
        );
    }
    
        public function form($instance){?>
          
          <label for="<?php echo $this->get_field_id('postType'); ?>">Write Post Type</label>
          <input type="text" name="<?php echo $this->get_field_name('post_type'); ?>" id="<?php echo $this->get_field_id('postType'); ?>" value="<?php echo $instance['post_type'] ?>" class="widefat">
           
             <label for="<?php echo $this->get_field_id('PostPerPage'); ?>">Per Page Number</label>
          <input type="text" name="<?php echo $this->get_field_name('post_per_page'); ?>" id="<?php echo $this->get_field_id('PostPerPage'); ?>" value="<?php echo $instance['post_per_page'] ?>" class="widefat">
          
          <?php   }
    
            public function update($new,$old){
                
                $old['post_type'] = $new['post_type'];
                $old['post_per_page'] = $new['post_per_page'];
                return $old;

            }        
    
    
    
            public function widget( $args, $instance ) {
                
                $post_query = null;
                       $post_query = new WP_Query( array(
                       'post_type' => $instance['post_type'],
                        'posts_per_page' => $instance['post_per_page']
                       
                      ));
                if($post_query->have_posts()){
                    while($post_query->have_posts()){
                        $post_query->the_post();?>
                        
                        <ul>
                        <li><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></li>
                      
                        </ul>
                        
                 <?php       
                      }
                }
               wp_reset_postdata(); 
               $post_query = null;      
            }
    
            

}





function office_widgets(){
    
    register_sidebar( 
           
    array(
        
        'name' => 'Office Right SideBar',
        'id' => 'office_right_sidebar'
    
        )              
    );
    
    
    register_widget('OfficeSlider');
    register_widget('OfficePost');
    
}

add_action('widgets_init','office_widgets');



?>
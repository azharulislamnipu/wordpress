<?php

function myslider($para ,$content){
   $para = shortcode_atts(array(
    
            'fz' => '25px',
            'color' => 'red'
    
    
            ),$para); ob_start();?>
    
      <h2 style="color:<?php echo $para['color']; ?>; font-size:<?php echo $para['fz']?>"> <?php echo $content; ?> </h2>
<?php 
    return ob_get_clean();
}
add_shortcode('slider','myslider');


function office_slider($para ,$content){
   $para = shortcode_atts(array(
    
            'id' => 0
    
    
            ),$para); ob_start();?>
    
         <div id="carousel-example-generic<?php echo $para['id']; ?>" class="carousel slide" data-ride="carousel">
                       
                       
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                           
                        <?php 
                         $office_custom_post = null;
                          
        
                            $office_custom_post = new WP_Query(array(
                            'post_type'=>'slider',
                            'posts_per_page'=>-1,
                                'order' => 'ASC'

                            ));

                            if($office_custom_post->have_posts()){
                                $x=0;
                                while($office_custom_post->have_posts()){
                                    $x++;
                                    $office_custom_post->the_post();
                                   
                                    ?>
                                    
                                     <!-- Begin single Slide item-->
                                    <div class="item <?php if($x==1){ echo 'active';} ?>">
                                        <?php the_post_thumbnail('slide-img'); ?>
                                        
                                    </div>
                                    <!-- End single Slide item -->
                                   
                                    
                              <?php  }
                                
                            }else{
                                echo "no post";
                            }
                        
                         wp_reset_postdata();

                        ?>
                           
                            
                        </div>
                        <!-- Controls -->
                        
                         <!-- Indicators -->
                        <ol class="carousel-indicators">
                           
                           
                           <?php
                        for($i=0;$i<$x;$i++){ ?>
                           
                    <li data-target="#carousel-example-generic<?php echo $para['id']; ?>" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0){ echo 'active';} ?>"></li>
                           
                           <?php  }
 
                        ?> 
                           
                           
                        </ol>
                        
                        
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic<?php echo $para['id']; ?>" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic<?php echo $para['id']; ?>" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                        
         </div>        
    
            
            
            
       
<?php    return ob_get_clean();
}
add_shortcode('myslider','office_slider');
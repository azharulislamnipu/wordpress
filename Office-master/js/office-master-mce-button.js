jQuery( document ).ready(function( $ ) {
                             
                                    	tinymce.PluginManager.add('office_first_button', function( editor, url ) {
                                        editor.addButton('office_first_button', {
                                                text: 'Office first button',
                                                icon: false,
                                                onclick: function() {
                                                    editor.insertContent('[slider]');
                                                }
                                            });
                                        });
    
    
    
    
    
                                        tinymce.PluginManager.add('office_second_button', function( editor, url ) {
                                        editor.addButton('office_second_button', {
                                                text: 'Office second button',
                                                icon: false,
                                                type:'menubutton',
                                                menu:[
                                                    {
                                                        text:'Menu first Level 1',
                                                        onclick: function() {
                                                        editor.insertContent('[myslider]');
                                                        }
                                                    },
                                                     {
                                                        text:'Menu first Level 2',
                                                         
                                                        menu:[
                                                                {
                                                                  text:'Menu 2nd Level 1',
                                                                    onclick: function() {
                                                                    editor.insertContent('[myslider]');
                                                                    }

                                                                },
                                                                
                                                                 {
                                                                  text:'Menu 2nd Level 2',
                                                                    onclick: function() {
                                                                    editor.insertContent('[myslider]');
                                                                    }

                                                                }
                                                            ]
                                                       
                                                    },
                                                     {
                                                        text:'Menu first Level 3',
                                                     
                                                         onclick: function() {
                                                                editor.windowManager.open( {
                                                                    title: 'Insert Your Form  Shortcode',
                                                                    body: [
                                                                        {
                                                                            type: 'textbox',
                                                                            name: 'firstName',
                                                                            label: 'First Name',
                                                                            
                                                                        },
                                                                        {
                                                                            type: 'textbox',
                                                                            name: 'address',
                                                                            label: 'Address',
                                                                            
                                                                            multiline: true,
                                                                            minWidth: 400,
                                                                            minHeight: 150
                                                                        },
                                                                        {
                                                                            type: 'listbox',
                                                                            name: 'selectSex',
                                                                            label: 'Sex',
                                                                            'values': [
                                                                                {text: 'Male', value: 'male'},
                                                                                {text: 'Female', value: 'female'}
                                                                               
                                                                            ]
                                                                        }
                                                                    ],
                                                                    onsubmit: function( x ) {
                                                                        editor.insertContent(
                                                                        
                                                                        '[firstName="'+x.data.firstName+'" address="'+x.data.address+'" sex="'+x.data.selectSex+'"]'
                                                                        
                                                                        
                                                                        );
                                                                    }
                                                                });
                                                            }
                                                    }
                                                ]
                                                
                                            });
                                        });


                                    
                                }
                            );



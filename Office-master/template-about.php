<?php get_header();
/*
  Template Name: For about page  
    
*/
$prf= '_office_master_';
if(have_posts()){
    the_post();
  $page_thumb = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ) , 'full' );
    $content = get_the_content();
    $group_meta_data = get_post_meta(get_the_ID(),$prf.'about_group_meta_field',true);
?>
     
      <div class="row container-kamn">  
        <img src="<?php echo $page_thumb[0];?>" width="100%" class="blog-post" alt="Feature-img" align="right" width="100%"> 
    </div>
 
    <!--End Header -->
    
<?php }


?>


    <div id="banners"></div>
    <div class="container">   
        <div class="row">
            <div class="side-left col-sm-4 col-md-4">
                <?php  foreach($group_meta_data as $single){ ?>
                     
               
                <h3 class="lead"> <?php echo $single[$prf.'heading'];?></h3><hr>

                <p><?php echo $single[$prf.'about_description']; ?></p>
                
                <?php if(is_array($single[$prf.'hash_link'])){
    
                    foreach($single[$prf.'hash_link'] as $key =>$childsingle){ ?>
                
                <a href="<?php echo $childsingle; ?>"> <?php echo $single[$prf.'hash_link_title'][$key]; ?></a><br>
                
                
                <?php } } ?>
                <br>
                 
                
                      
                      
                <?php  }  ?>

                
                
            </div>
            <div class="col-sm-8 col-md-8">
             
             <?php
                  echo $content;
               
                 
               ?>
              
            </div>  
        </div>    
    </div>  

    <!--End Main Container -->

  <?php get_footer();?>
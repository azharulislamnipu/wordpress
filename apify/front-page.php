<?php get_header(); ?>

<h1 style="text-align:center"> This is our welcome page</h1>

<?php
 wp_nav_menu( array(
 
     'theme_location' => 'primary_menu'
 
 
 ));

?>


<section class="search_form">
    <div class="search">
        <?php get_search_form();?>
        
        
    </div>
</section>

<section class="sideba_area">
    <div class="sidebar">
       <?php
        if(!dynamic_sidebar('apify_first_widget')){
            echo "not widget here";
        }
        ?>
        
    </div>
    
    <div class="sidebar">
       <?php
        if(!dynamic_sidebar('apify_2nd_widget')){
            echo "not widget here";
        }
        ?>
        
    </div>
</section>

<section class="custom_post" style="background-color:#93D5F3;border:2px solid #871F09; padding:50px 20px;margin-bottom:30px;">
    <div class="all_post">
    <h1 style="text-align:center">Default Post Here</h1>
    <?php
        
        $apify_custom_post = null;
        
        $apify_custom_post = new WP_Query(array(
        'post_type'=>'post',
        'posts_per_page'=>'5',
        'category_name' => 'laptop,products'
        
        ));
        
        if($apify_custom_post->have_posts()){
            while($apify_custom_post->have_posts()){
                $apify_custom_post->the_post();
              
               
        
            ?> 
                <div class="single_post" style="border:2px solid gray; margin-top:5px;">
                    
                    <h2><a href="<?php echo the_permalink(); ?>"> <?php echo the_title();?></a></h2>
                  
                    
                </div>
                
          <?php  }
        }else{
            echo "No post";
        }
        
        wp_reset_postdata();
        
        
    ?>
    
    
    </div>
</section>



<section class="custom_post" style="background-color:#F78A48;border:2px solid green; padding:50px 20px;">
    <div class="all_post">
    <h1 style="text-align:center">All Pruducts</h1>
    <?php
        $apify_custom_post = null;
        $apify_custom_post = new WP_Query(array(
        'post_type'=>'product_post',
        'posts_per_page'=>'5'
        
        ));
        
        if($apify_custom_post->have_posts()){
            while($apify_custom_post->have_posts()){
                $apify_custom_post->the_post();
               $Name = get_post_meta(get_the_ID(),'name',true);
               $mobile = get_post_meta(get_the_ID(),'Mobile',true);
        
            ?>
               
                <div class="single_post" style="border:2px solid black; margin-top:5px;">
                    
                    <h2><a href="<?php echo the_permalink(); ?>"> <?php echo the_title();?></a></h2>
                    <h3> <?php echo $Name; ?></h3>
                    <h3> <?php echo $mobile; ?></h3>
                </div>
                
          <?php  }
        }else{
            echo "No post";
        }
        
        wp_reset_postdata();
   
        
        
    ?>
    </div>
</section>


<section class="custom_post" style="background-color:#28E352;border:2px solid blue; padding:50px 20px;margin-top:30px;">
    <div class="all_post">
    <h1 style="text-align:center">All Service</h1>
    <?php
        
        $apify_custom_post = null;
        
        $apify_custom_post = new WP_Query(array(
        'post_type'=>'service',
        'posts_per_page'=>'5'
        
        ));
        
        if($apify_custom_post->have_posts()){
            while($apify_custom_post->have_posts()){
                $apify_custom_post->the_post();
               $service = get_post_meta(get_the_ID(),'service',true);
               
        
            ?> 
                <div class="single_post" style="border:2px solid gray; margin-top:5px;">
                    
                    <h2><a href="<?php echo the_permalink(); ?>"> <?php echo the_title();?></a></h2>
                    <h3> <?php echo $service; ?></h3>
                    
                </div>
                
          <?php  }
        }else{
            echo "No post";
        }
        
        wp_reset_postdata();
        
        
    ?>
    
    
    </div>
</section>




<?php get_footer(); ?>
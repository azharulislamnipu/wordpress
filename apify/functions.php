<?php
 

function apify_thme_init(){
    
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('post-thumb', 600, 400, true);
    add_image_size('search-thumb', 300, 200, true);
    
    register_nav_menus(array( 
        
        'header_menu' => 'Main Menu',
        'footer_menu' => 'Bottom Menu',
        'primary_menu' => 'primary Menu'
    
    ));
    
    add_theme_support('html5');
    

}

add_action('after_setup_theme','apify_thme_init');

function apify_script(){
    
    wp_enqueue_style('main_style',get_stylesheet_uri(),array('apify-style'),'v.4.78','all');
    
    wp_enqueue_style('apify-style',get_template_directory_uri().'/css/test.css',null, 'v.1.204', 'all');

    wp_enqueue_script('apify-script',get_template_directory_uri().'/js/script.js',array('jquery'),'v.1.230',true);
    
    wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'apify_script');


function apify_widgets(){
    register_sidebar(array(
    
        'name' => 'First Widget',
        'id' => 'apify_first_widget'
    
    ));
    
     register_sidebar(array(
    
        'name' => 'Second Widget',
         'id' => 'apify_2nd_widget'
    
    ));
    
}
add_action('widgets_init','apify_widgets');

function apify_custom_posts(){
    register_post_type('product_post',array(
    
    'labels' => array(
        'name' => 'Apify Products',
        'menu_name' => 'Apifiy Menu',
        'all_items' => 'All Products',
        'add_new' => 'Add New Product',
        'add_new_item' => 'Add New Product'
         ),
    'public'=>true,
    'supports' => array(
        'title','editor','thumbnail','excerpt','revisions','custom-fields','page-attributes'
     )
    ));
    
     register_post_type('service',array(
    
    'labels' => array(
        'name' => 'Apify Service',
        'menu_name' => 'Service Menu',
        'all_items' => 'All Service',
        'add_new' => 'Add New Service',
        'add_new_item' => 'Add New Serivce'
         ),
    'public'=>true,
    'supports' => array(
        'title','editor','thumbnail','excerpt','revisions','custom-fields','page-attributes'
     )
    ));
    
}
add_action('init','apify_custom_posts')































?>